<!doctype html>
<html lang="es" xml:lang="es" class="no-js">
<head>
	<title>Contacto</title>
	<?php include('contenido/head.php'); ?>
</head>
<body><em></em>

	<?php include('chat.php'); ?>

	<!-- Container -->
	<div id="container">
		<?php include('contenido/header.php'); ?>
		<?php include('contenido/analytics.php'); ?>
		<div id="content">

			<!-- Page Banner -->
			<div class="page-banner">
				<div class="container">
					<h2>Contáctanos</h2>

				</div>
			</div>

			<!-- contact box -->
			<div class="contact-box">
				<div class="container">
					<div class="row">

						<div class="col-md-6" align="center">

						 
						<div class="container">
							<div class="col-md-12" >
								<?php include('jotformadspost.php'); ?>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="contact-information">
							<h3>Horario de Atención</h3>
							<p>Con gusto esperamos tu llamada en nuestro <strong>Call Center</strong>, para cualquier duda, aclaración o sugerencia que quieras compartirnos en <strong>FAME Honda Marquesa</strong>; te escuchamos y atendemos de manera personalizada. </p>
							<p class="work-time"><span>Lunes - Viernes</span> : 7:00 a.m. - 7:00 p.m. | <span>Sábado</span> : 8:00 a.m. - 2:00 p.m.</p>
						</div>
						<div class="contact-information">
							<h3>Información de Contacto</h3>
							<ul class="contact-information-list">
								<li><span><i class="fa fa-home"></i>Blvd. Bernardo Quintana #622 Col. Desarrollo San Pablo C.P 76130 Querétaro, Qro</span></li>
								<li><span><i class="fa fa-phone"></i>Agencia : <strong>(442) 209 7900</strong></span></li>
								<li>
									<i class="fa fa-phone"></i><span>NÚMERO CONMUTADOR:<strong> (442) 209-7900</strong></span><br>
									<i class="fa fa-phone"></i><span>POSTVENTA:	<strong> (442) 209-7910</strong></span><br>
									<i class="fa fa-phone"></i><span>REFACCIONES: <strong> (442) 209-7909</strong></span><br>
									<i class="fa fa-phone"></i><span>CREDITO Y SEGUROS: <strong> (442) 209-7915</strong></span><br> <i class="fa fa-phone"></i><span>GERENCIA GENERAL: <strong> (442) 209-7911</strong></span><br> 
									<i class="fa fa-phone"></i><span>RECEPCION: <strong>(442) 209-7900  *presionar 0</strong></span><br> 
									<i class="fa fa-phone"></i><span>SEMINUEVOS: <strong>(442) 209-7900 *presionar 4</strong></span><br> 
									<i class="fa fa-phone"></i><span>TALLER DE SERVICIO CITAS: <strong> (442) 209-7912 | (442) 209-7913 | (442) 209-7914</strong></span><br>   
									<i class="fa fa-phone"></i><span>VENTAS: <strong> (442) 209-7901 | (442) 209-7902 | (442) 209-7911</strong></span><br>       
								</li>
								<h3>Whatsapp</h3>
								<li>
									<span>
										<i class="fa fa-whatsapp"></i> CITAS SERVICIO: 
										<strong>
											<a href="https://api.whatsapp.com/send?phone=524432026580&text=Hola,%20Quiero%20más%20información!" title="Cita de Servicio">
												443-202-6580
											</a>
										</strong>
										<br>
										<i class="fa fa-whatsapp"></i> REFACCIONES: 
										<strong>
											<a href="https://api.whatsapp.com/send?phone=524432732778&text=Hola,%20Quiero%20más%20información!" title="Refacciones">
												443-273-2778
											</a>
										</strong>
										<br>
										<i class="fa fa-whatsapp"></i> ATENCIÓN A CLIENTES: 
										<strong>
											<a href="https://api.whatsapp.com/send?phone=524432294955&text=Hola,%20Quiero%20más%20información!" title="Atencion a Clientes">
												443-229-4955
											</a>
										</strong>
										<br>
										<i class="fa fa-whatsapp"></i> VENTAS NUEVOS:
										<strong>
											<a href="https://api.whatsapp.com/send?phone=524425923862&text=Hola,%20Quiero%20más%20información!" title="Ventas Nuevos">
												442 592 38 62
											</a>

										</strong>
										<br>
										<i class="fa fa-whatsapp"></i> VENTAS USADAS:
										<strong>
											<a href="https://api.whatsapp.com/send?phone=524432182765&text=Hola,%20Quiero%20más%20información!" title="Ventas Usados">
												443-218-2765
											</a>
										</strong>
										<br>
									</span>
								</li>

								<li><a href="#"><i class="fa fa-envelope"></i>recepcion@famemarquesa.com</a></li>
							</ul>
						</div>
					</div>
					

				</div>
			</div>
		</div>

	</div><br>

	<?php include('contenido/footer.php'); ?>
</div> 			

</body>
</html>